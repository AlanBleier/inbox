// This standard module will help us create a path fromt he compile.js file to the inbox.sol
// It also helps us guarantee cross-plataform compatibility
// require('.contracts/Inbox.sol') <-- this is not the way to do it!
const path = require('path');
const fs = require('fs');
const solc = require('solc');

// The path where the actual file resides
// __dirname is a constant defined by node, set to the current working directory
// contracts is the folder, Inbox.sol is the actual file
const inboxPath = path.resolve(__dirname, 'contracts', 'Inbox.sol');

// reads the contents of the file, the actual raw source code
const source = fs.readFileSync(inboxPath, 'utf8');

// Ths function is given the source code and the number of files we want to compile
// Wrapping it around console.log() will give us a better idea of what's happening behind the scenes
// console.log(solc.compile(source, 1));
// Compile with node compile.js in a terminal located at the root directory of the file
// that is, /home/bleier/Documents/code/inbox

module.exports = solc.compile(source, 1).contracts[':Inbox'];
